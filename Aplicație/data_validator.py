from datetime import datetime

class Data_Validator:
    @staticmethod
    def valid_name(name):
        if len(name) < 1 or len(name) > 50:
            return False
        for char in name:
            if not (char.isalpha() or char == '-'):
                return False
        return True
        
    @staticmethod
    def valid_phone_nr(phone_nr):
        if len(phone_nr) != 10 or not phone_nr.isdigit():
            return False
        if not phone_nr.startswith("07"):
            return False
        return True
    
    @staticmethod
    def valid_adress(adress):
        if len(adress) < 5 or len(adress) > 50:
            return False
        
        return True
    
    @staticmethod
    def valid_email(email: str):
        if len(email) < 5 or len(email) > 50:
            return False
        if email.count('@') != 1:
            return False
        if '.' not in email.split('@')[1]:
            return False
        return True
    
    @staticmethod
    def valid_CNP(cnp: str):
        if len(cnp) != 13 or not cnp.isdigit():
            return False
        
        if Data_Validator.get_gender(cnp) is None:
            return False

        d = Data_Validator.get_b_day(cnp)
        m = Data_Validator.get_b_month(cnp)
        
        if m < 1 or m > 12:
            return False
        
        if d < 1 or d > 31:
            return False
        
        return True
        
    @staticmethod
    def get_gender(cnp: str):
        val = int(cnp[0])
        if val == 1 or val == 2:
            return 'M'
        
        if val == 5 or val == 6:
            return 'F'
        return None
        
    @staticmethod
    def get_b_day(cnp: str):
        return int(cnp[5:7])
    
    @staticmethod
    def get_b_month(cnp: str):
        return int(cnp[3:5])
    
    @staticmethod
    def get_b_year(cnp: str):
        return int(cnp[1:3])

    @staticmethod
    def check_all_pacient_data(
        f_name, l_name, cnp, phone_nr, adress, email
    ):
        if not Data_Validator.valid_name(f_name):
            return False
        if not Data_Validator.valid_name(l_name):
            return False
        if not Data_Validator.valid_CNP(cnp=cnp):
            return False
        if not Data_Validator.valid_phone_nr(phone_nr):
            return False
        if not Data_Validator.valid_adress(adress):
            return False
        if not Data_Validator.valid_email(email):
            return False
        return True
    
    @staticmethod
    def check_all_consult_data(pacient_code, treatment, observations, ):
        if not Data_Validator.valid_CNP(pacient_code):
            return False
        if len(treatment) < 2 or len(treatment) > 350:
            return False 
        if len(observations) > 350:
            return False 
        return True

    @staticmethod
    def get_age_from_cnp(cnp):
        y = int(cnp[1:3])
        m = int(cnp[3:5])
        d = int(cnp[5:7])
        curr_date = datetime.now()
        y_val = int(cnp[0])
        b_y = 1900 + 100 * (y_val > 2) + y  
        age = curr_date.year - b_y

        # Adjust age
        if curr_date.month < m or curr_date.month == m and curr_date.day < d:
            age -= 1
            
        return age
        