import tkinter as tk
from settings import Settings
from PIL import Image
from data_validator import Data_Validator
import io

class GalleryView:
    def __init__(self, root: tk.Tk, session_list) -> None:
        self.root = root
        self.session_list = session_list
        self.window = tk.Toplevel(self.root)
        self.window.geometry('800x600')
        self.window.config(
            bg=Settings.getColorPallete()['col1']
        )
        self.window.rowconfigure(0, weight=9)
        self.window.rowconfigure(1, weight=1)
        self.window.columnconfigure((0, 1), weight=1)
        self.consultation_index = 0

        self.back_btn = tk.Button(
            self.window, text='Înapoi', 
            command=self.__gallery_back_btn_action
        )
        self.back_btn.grid(
            row=1, column=0, 
            sticky='nws'
        )
        self.next_btn = tk.Button(
            self.window, text='Înainte', 
            command=self.__gallery_next_btn_action
        )
        self.next_btn.grid(
            row=1, column=1, 
            sticky='nes'
        )

        self.previous_gallery_frame: tk.Frame = None

        if len(self.session_list) > 0:
            self.__set_gallery_frame()
        else:
            self.__set_empty_gallery_frame()
        
    def __gallery_back_btn_action(self):
        self.consultation_index-=1
        if self.consultation_index < 0:
            self.consultation_index = len(self.session_list) - 1;
        self.__set_gallery_frame()


    def __gallery_next_btn_action(self):
        self.consultation_index+=1
        if self.consultation_index == len(self.session_list):
            self.consultation_index = 0;
        self.__set_gallery_frame()

    def __set_gallery_frame(self):
        new_frame = self.__get_gallery_frame()
        new_frame.grid(
            row=0, column=0, columnspan=2, 
            sticky='news'
        )

    def __view_xray_btn_action(self):
        # Get xray BLOB
        bin_img_data = self.session_list[self.consultation_index][3]
        
        img = Image.open(io.BytesIO(bin_img_data))
        img.show()

    def __get_gallery_frame(self):
        label_font = Settings.getMainFont(20)

        gallery_frame = tk.Frame(
            self.window,
            bg=Settings.getColorPallete()['col5']
        )
        if self.previous_gallery_frame is not None:
            self.previous_gallery_frame.destroy()
            self.previous_gallery_frame = gallery_frame

        # Setup grid
        gallery_frame.rowconfigure([i for i in range(5)], weight=1)
        gallery_frame.columnconfigure([i for i in range(4)], weight=1)

        # diagnostic pe baza modelului de predictie
        diagnostic = Settings.get_diagnostic_map()[
            self.session_list[self.consultation_index][4]
        ]

        # Add widgets
        tk.Label(
            gallery_frame, 
            text=f'Diagnostic: {diagnostic}',
            font=label_font,
            bg=Settings.getColorPallete()['col1']        
        ).grid(row=0, column=0, columnspan=2, sticky='news')
        tk.Label(
            gallery_frame, 
            text=f'Tratament',
            font=label_font        
        ).grid(row=3, column=0, sticky='news')
        tk.Label(
            gallery_frame, 
            text=f'Observații',
            font=label_font        
        ).grid(row=3, column=1, sticky='news')
        
        tk.Label(
            gallery_frame, 
            text=f'Adăugat la: {self.session_list[self.consultation_index][2]}',
            font=label_font        
        ).grid(row=1, column=0, sticky='new')
        
        session_cnp = self.session_list[self.consultation_index][10]

        info_frame = tk.Frame(
            gallery_frame,
            bg=Settings.getColorPallete()['col5']
        )
        info_frame.grid(row=2, column=0, columnspan=1, sticky='news')

        tk.Label(
            info_frame, 
            text=f"Sexul: {Data_Validator.get_gender(session_cnp)}", 
            font=label_font,
            bg=Settings.getColorPallete()['col5']
        ).pack(side='left', padx=(0, 20))

        tk.Label(
            info_frame, text=f"Vârsta: {Data_Validator.get_age_from_cnp(session_cnp)}", 
            font=label_font,
            bg=Settings.getColorPallete()['col5']
        ).pack(side='left')

        treatment_text = tk.Text(
            gallery_frame, font=label_font,height=4,
            padx=2
        )
        treatment_text.insert(tk.END, self.session_list
        [self.consultation_index][5])

        treatment_text.config(
            state=tk.DISABLED
        )
        treatment_text.grid(row=4, column=0, sticky='w')

        observations_text = tk.Text(
            gallery_frame, font=label_font,height=4,
            padx=2
        )
        observations_text.insert(tk.END, self.session_list[self.consultation_index][6])
        observations_text.config(
            state=tk.DISABLED
        )
        observations_text.grid(row=4, column=1, sticky='e')

        # Adauga buton de vizionare radiografie
        tk.Button(
            gallery_frame, text="Vizualizare radiografie",
            command=self.__view_xray_btn_action, 
            bg=Settings.getColorPallete()['col2'],
            font=label_font
        ).grid(row=1, column=1, sticky='ne')

        return gallery_frame
    
    # atunci cand nu exista istoric pt pacient
    def __set_empty_gallery_frame(self):

        self.next_btn.destroy()
        self.back_btn.destroy()

        tk.Label(
            self.window,text='Nimic de afișat',
            pady = 50,
            font=Settings.getMainFont(30)
        ).pack(fill='x')