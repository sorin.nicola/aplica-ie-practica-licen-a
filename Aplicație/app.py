import tkinter as tk
from settings import Settings
from menu import Menu


class App:
    def __init__(self) -> None:
        self.window = tk.Tk()
        self.window.configure(bg=Settings.getColorPallete()['col1'])
        self.menu = Menu(self.window)

    def run(self):
        self.window.rowconfigure(0, weight=1)
        self.window.rowconfigure(1, weight=9)
        self.window.columnconfigure(0, weight=1)
        self.window.geometry(Settings.get_size_str())

        # Add menu to screen
        self.menu.set_menu_scene()
        self.menu.set_default_content_frame(0)
        self.window.mainloop()
        
if __name__ == '__main__':
    app = App()
    app.run()
