import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter import messagebox
import antrenare
from os.path import basename
from settings import Settings
from database_connector import DatabaseConnector
from data_validator import Data_Validator

class AddConsultScene:
    def __init__(self, window:tk.Tk) -> None:
        self.window = window
        self.db_connector = DatabaseConnector()
        self.main_frame = tk.Frame(
            self.window, bg=Settings.getColorPallete()['col5'], padx=50, pady=50
        )
        self.curr_xray_frame: tk.Frame = None
        self.img_path_label = tk.Label(
            self.main_frame,
            bg=Settings.getColorPallete()['col5'],
            font=Settings.getMainFont(25)
        )
        self.img_path_label.grid(
            row=1, column=1, sticky='nw',
        )

        self.diagnostic_map = Settings.get_diagnostic_map()
        self.treatment_map = Settings.get_treatment_map()

        self.code_entry = tk.Entry(
            self.main_frame, text='CNP',
            font=Settings.getMainFont(25)
        )
        self.code_entry.grid(
            row=0, column=1, sticky='wn'
        )
        self.img_path = None

    def __save_consult_btn_action(self, pacient_code, img_path, diagnostic, treatment, observations):
        query = f"SELECT * from pacients WHERE cnp='{pacient_code}'"
        if Data_Validator.check_all_consult_data(
            pacient_code, treatment, observations
        ):
            if self.db_connector.get_data(query) is not None:  
                with open(img_path, 'rb') as file:
                    bin_data = file.read()
                query = f"SELECT pacient_id from pacients WHERE cnp = '{pacient_code}'"
                pacient_id = self.db_connector.get_data(query)[0]

                query = ("INSERT INTO consultations(pacient_id, xray, diagnostic, treatment, observations) "
                        "VALUES (%s, %s, %s, %s, %s)")
                params = (pacient_id, bin_data, diagnostic, treatment, observations)
                self.db_connector.execute_query(query, params)
                
                # curata campurile introduse
                self.curr_xray_frame.destroy()
                self.code_entry.delete(0, tk.END)
                self.img_path_label.config(text='')
            else:
                messagebox.showerror(
                    title='Eroare',
                    message='Pacientul nu a fost găsit'
                )
        else:
            messagebox.showerror(
                title='Eroare',
                message='Date invalide'
            )
        
        
    def __get_xray_frame(self, prediction):
        frame = tk.Frame(
            self.main_frame, bg=Settings.getColorPallete()['col5']
        )

        frame.rowconfigure((0, 1, 2, 3), weight=1)
        frame.columnconfigure((0, 1), weight=1)

        # Add diagnostic label
        tk.Label(
            frame, 
            text=f'Diagnostic: {self.diagnostic_map[prediction]}',
            bg=Settings.getColorPallete()['col5'],
            font=Settings.getMainFont(25)
        ).grid(
            row=0, column=0, sticky='nw',
        )
        # Add treatment label
        tk.Label(
            frame, 
            text=f'Tratament',
            bg=Settings.getColorPallete()['col5'],
            font=Settings.getMainFont(20)
        ).grid(
            row=1, column=0, sticky='nw',
        )
        # Add observations label
        tk.Label(
            frame, 
            text=f'Observatii',
            bg=Settings.getColorPallete()['col5'],
            font=Settings.getMainFont(20)
        ).grid(
            row=1, column=1, sticky='nw',padx=(10, 0)
        )

        treatment_text = tk.Text(
            frame, height=4,
            font=Settings.getMainFont(20)
        )
        treatment_text.grid(
            row=2, column=0, 
            padx=(0, 10), sticky='n'
        )
        treatment_text.insert(tk.END, self.treatment_map[prediction])
        observations_text = tk.Text(
            frame, height=4,
            font=Settings.getMainFont(20)
        )
        observations_text.grid(
            row=2, column=1, 
            padx=(10,0), sticky='n'
        )

        # buton salvare consult
        tk.Button(
            frame, text='Salvează Consultație',
            bg=Settings.getColorPallete()['col1'],
            fg=Settings.getColorPallete()['col4'],
            font=Settings.getMainFont(25), 
            command=lambda: self.__save_consult_btn_action(
                self.code_entry.get(),
                self.img_path,
                prediction,
                treatment_text.get('1.0', tk.END),
                observations_text.get('1.0', tk.END),
            )
        ).grid(
            row=3, column=0, sticky='wn'
        )

        # Delete previous xray_frame if existing
        if self.curr_xray_frame is not None:
            self.curr_xray_frame.destroy()
        self.curr_xray_frame = frame
        return frame

    def __set_xray_frame(self, frame:tk.Frame):
        frame.grid(
            row=2, column=0, columnspan=2,
            sticky='news'
        )

    def __load_xray_btn_action(self):
        pickedfiletypes = (('png files', '*.png'), ('jpeg files', '*.jpeg'))

        # selectare path radiografie
        self.img_path = askopenfilename(
            initialdir='./data_set',
            filetypes=pickedfiletypes
        )
        # self.img_path = 'data_set/Severe/9065272L.png'
        if len(self.img_path) == 0:
            return
        self.img_path_label.config(
            text=basename(self.img_path)
        )
        prediction = antrenare.get_prediction(self.img_path)
        # prediction = 'Severe'
        xray_frame = self.__get_xray_frame(prediction)
        self.__set_xray_frame(xray_frame)

    
    # se intoarce la frameul principal
    def get_frame(self):
        self.main_frame.rowconfigure((0, 1), weight=1)
        self.main_frame.rowconfigure(2, weight=4)
        self.main_frame.columnconfigure((0, 1, 2), weight=1)

        tk.Label(
            self.main_frame, text='CNP',
            bg=Settings.getColorPallete()['col5'],
            font=Settings.getMainFont(25)
        ).grid(
            row=0, column=0, sticky='wn'
        )

        # Adauga buton de incarcare radiografie
        tk.Button(
            self.main_frame, text='Încarcă Radiografie',
            bg=Settings.getColorPallete()['col1'],
            fg=Settings.getColorPallete()['col4'],
            font=Settings.getMainFont(25), 
            command=self.__load_xray_btn_action
        ).grid(
            row=1, column=0, sticky='wn'
        )

        return self.main_frame
