import cv2
import numpy as np
import tensorflow as tf
best_model= tf.keras.models.load_model('best_model.keras')


def get_prediction(img_path):
    best_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    img_size = 224
    # Definirea categoriilor sau claselor pentru predicție
    categories = ['Doubtful', 'Healthy', 'Minimal', 'Moderate', 'Severe']  
    # image_path = r'./test.png' 

    # Încarcă imaginea
    img_single = cv2.imread(img_path)

    # Redimensionează imaginea la dimensiunea dorită
    img_single = cv2.resize(img_single, (img_size, img_size))

    # Converteste imaginea la alb-negru (grayscale) dacă modelul așteaptă acest tip de imagine
    img_single_gray = cv2.cvtColor(img_single, cv2.COLOR_BGR2GRAY)

    # Normalizează imaginea dacă este necesar (de exemplu, la intervalul [0, 1])
    img_single_gray_norm = img_single_gray / 255.0

    # Adaugă dimensiunea de batch (1, 224, 224, 1) pentru imaginea alb-negru (grayscale)
    img_single_batch = np.expand_dims(img_single_gray_norm, axis=(0, -1))

    # Realizează predicția
    predictions_single = best_model.predict(img_single_batch)

    return categories[np.argmax(predictions_single)]