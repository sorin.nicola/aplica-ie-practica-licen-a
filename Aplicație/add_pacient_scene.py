import tkinter as tk
from settings import Settings
from data_validator import Data_Validator
from database_connector import DatabaseConnector

class AddPacientScene:
    def __init__(self, window: tk.Tk) -> None:
        self.window = window
        self.frame = tk.Frame(self.window, bg=Settings.getColorPallete()['col5'])
        self.db_connector = DatabaseConnector()


        # Adaugare intrari
        self.f_name_entry = tk.Entry(
            self.frame, font=Settings.getMainFont(25)
        )
        self.f_name_entry.grid(row=0, column=1, sticky='news', padx=(100, 50), pady=30)
        
        self.l_name_entry = tk.Entry(
            self.frame, font=Settings.getMainFont(25)
        )
        self.l_name_entry.grid(row=1, column=1, sticky='news', padx=(100, 50), pady=30)

        self.code_entry = tk.Entry(
            self.frame, font=Settings.getMainFont(25)
        )
        self.code_entry.grid(row=2, column=1, sticky='news', padx=(100, 50), pady=30)

        self.phone_entry = tk.Entry(
            self.frame, font=Settings.getMainFont(25)
        )
        self.phone_entry.grid(row=3, column=1, sticky='news', padx=(100, 50), pady=30)
        self.adress_entry = tk.Entry(
            self.frame, font=Settings.getMainFont(25)
        )
        self.adress_entry.grid(row=4, column=1, sticky='news', padx=(100, 50), pady=30)

        self.email_entry = tk.Entry(
            self.frame, font=Settings.getMainFont(25)
        )
        self.email_entry.grid(row=5, column=1, sticky='news', padx=(100, 50), pady=30)

        self.error_label = tk.Label(
            self.frame, text='Date invalide',
            bg=Settings.getColorPallete()['col5'],
            fg='red', font=Settings.getMainFont(20)
        )
        self.error_label.grid(row=6, column=0)
        self.error_label.grid_remove()

        self.success_label = tk.Label(
            self.frame, text='Succes',
            bg=Settings.getColorPallete()['col5'],
            fg='green', font=Settings.getMainFont(20)
        )
        self.success_label.grid(row=6, column=0)
        self.success_label.grid_remove()

    def __load_labels(self, frame: tk.Frame, size):
        label_tags = (
            'Nume', 'Prenume', 'CNP',
            'Telefon', 'Adresă', 'Email'
        )

        for i in range(len(label_tags)):
            tk.Label(
                self.frame, text=label_tags[i],
                font=Settings.getMainFont(size)
            ).grid(
                row=i, column=0, sticky='ewns', 
                padx=(50, 0), pady=30
            )

    
    def __add_btn_event(self):
        if Data_Validator.check_all_pacient_data(
            self.f_name_entry.get(),
            self.l_name_entry.get(),
            self.code_entry.get(),
            self.phone_entry.get(),
            self.adress_entry.get(),
            self.email_entry.get()
        ):
            self.error_label.grid_remove()
            self.success_label.grid()
            self.db_connector.add_pacient(
                self.f_name_entry.get(),
            self.l_name_entry.get(),
            self.code_entry.get(),
            self.phone_entry.get(),
            self.adress_entry.get(),
            self.email_entry.get()
            )
            #Clear entry fields after adding pacient
            self.f_name_entry.delete(0, tk.END)
            self.l_name_entry.delete(0, tk.END)
            self.code_entry.delete(0, tk.END)
            self.phone_entry.delete(0, tk.END)
            self.adress_entry.delete(0, tk.END)
            self.email_entry.delete(0, tk.END)


        else:
            self.success_label.grid_remove()
            self.error_label.grid()


    def get_frame(self):

        # set frame grid layout
        self.frame.rowconfigure([i for i in range(7)], weight=1)
        self.frame.columnconfigure(0, weight=1)
        self.frame.columnconfigure(1, weight=3)

        # Load labels
        self.__load_labels(self.frame, 20)

        # Add Button
        tk.Button(
            self.frame, text="Adaugare", 
            font=Settings.getMainFont(30),
            bg=Settings.getColorPallete()['col2'],
            command=self.__add_btn_event
        ).grid(
            row=6, column=1, sticky='nse', 
            padx=(0, 50), pady=(0, 20)
        )

        return self.frame