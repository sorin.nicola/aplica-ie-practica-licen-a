import mysql.connector
import tkinter as tk
from tkinter.filedialog import askopenfilename


class DatabaseConnector:
    def __init__(self) -> None:
        self.db = mysql.connector.connect(
            host='localhost',
            user='root',
            passwd='root',       
            database='pacientDB'
        )
        self.cursor = self.db.cursor()


    def execute_query(self, query, params = None):
        try:
            self.cursor.execute(query, params)
            self.db.commit()  
        except Exception as e:
            print(e)

    def get_data(self, query, params = None):
        try:
            self.cursor.execute(query, params)
            return self.cursor.fetchone()
        except Exception as e:
            print(e)

    def add_pacient(self, f_name, l_name, cnp, phone_nr, adress, email):
        query = f"insert into pacients(f_name, l_name, cnp, phone_nr, adress, email) values ('{f_name}', '{l_name}', '{cnp}', '{phone_nr}', '{adress}', '{email}')"

        self.execute_query(query)


    def load_img(self):
        tk.Tk().withdraw()  # Hide the root Tkinter window
        f_path = askopenfilename(initialdir='./data_set')
        if f_path:
            with open(f_path, 'rb') as file:
                bin_data = file.read()
            query = 'INSERT INTO images (img) VALUES (%s)'
            self.execute_query(query, (bin_data,))


    def get_all_data(self, query, params = None):
        res_list = []
        res = self.get_data(query, params)
        while res is not None:
            res_list.append(res)
            res = self.cursor.fetchone()
        return res_list
