class Settings:
    win_width = 1200
    win_height = 800
    main_font = 'Comic Sans'

    @staticmethod
    def getColorPallete():
        return {"col1":'#27374D', "col2":'#526D82', "col3":'#dadada', "col4":'#DDE6ED', "col5":"#1B4F72"}

    @staticmethod
    def getMainFont(size):
            return (Settings.main_font, size)
    
    @staticmethod
    def get_size_str():
        return f'{Settings.win_width}x{Settings.win_height}'
    

    @staticmethod
    def get_diagnostic_map():
        return {
            'Doubtful': 'Osteoartrită grad 1',
            'Healthy': 'Osteoartrită grad 0', 
            'Minimal': 'Osteoartrită grad 2', 
            'Moderate': 'Osteoartrită grad 3', 
            'Severe': 'Osteoartrită grad 4'
        }
    
    @staticmethod
    def get_treatment_map():
        return {
            'Doubtful': "exerciții fizice cu impact redus menținerea unei greutăți corporale sănătoase pentru a reduce presiunea asupra articulațiilor",

            'Healthy': 'Nu este necesar', 

            'Minimal': 'Exerciții pentru întărirea mușchilor din jurul articulației afectate și administrarea de analgezice fără prescripție medicală, cum ar fi acetaminofenul, sau antiinflamatoare nesteroidiene (AINS), cum ar fi ibuprofenul.', 

            'Moderate': 'Fizioterapie intensivă: Program personalizat de exerciții pentru a îmbunătăți mobilitatea și forța, AINS pentru a reduce durerea și inflamația, injecții intra-articulare cu corticosteroizi pentru a reduce inflamația și durerea.',

            'Severe': ' AINS puternice sau opioide pentru controlul durerii, injecții cu acid hialuronic pentru lubrifierea articulației sau cu corticosteroizi, program de reabilitare intensivă iar daca aceste soluții nu dau rezultate, poate fi necesară o intervenție chirurgicală, cum ar fi artroplastia totală de genunchi'
        }
    