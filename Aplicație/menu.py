import tkinter as tk
from settings import Settings
from add_pacient_scene import AddPacientScene
from search_pacient_scene import SearchPacientScene
from add_consult_scene import AddConsultScene

class Menu:
    def __init__(self, window: tk.Tk) -> None:
        self.window = window
        self.menu_btn_list: list[tk.Button] = []
        self.add_pacient_frame = AddPacientScene(self.window).get_frame()
        self.current_frame: tk.Frame = None
        # Set default menu content scene
        self.default_frame = AddPacientScene(self.window).get_frame()
        

    def __load_menu_btn(self,menu_frame: tk.Frame, text, action: callable):
        b = tk.Button(  
            menu_frame, text=text, command=action,
            highlightthickness=0, bd=0, bg=Settings.getColorPallete()['col1'],
            font=Settings.getMainFont(30), padx=30
        )
        self.menu_btn_list.append(b)
        b.pack(side='left', fill='y')


    def __highlight_btn(self, btn_index, default_color, highlight_color):
        for i in range(len(self.menu_btn_list)):
            if i == btn_index:
                self.menu_btn_list[i].configure(bg = highlight_color)        
            else:
                self.menu_btn_list[i].configure(bg = default_color)        


    # Define menu button actions
    def __search_pacient_btn_action(self):
        self.__highlight_btn(
            0, 
            highlight_color=Settings.getColorPallete()['col5'], 
            default_color=Settings.getColorPallete()['col1'])
        self.__set_content_frame(
            SearchPacientScene(self.window).get_frame()
        )


    def __register_consult_btn_action(self):
        self.__highlight_btn(
            1, 
            highlight_color=Settings.getColorPallete()['col5'], 
            default_color=Settings.getColorPallete()['col1'])
        self.__set_content_frame(
            AddConsultScene(self.window).get_frame()
        )


    def __add_pacient_btn_action(self):
        self.__highlight_btn(
            2, 
            highlight_color=Settings.getColorPallete()['col5'], 
            default_color=Settings.getColorPallete()['col1'])
        self.__set_content_frame(
            AddPacientScene(self.window).get_frame()
        )


    def __get_menu_frame(self):
        menu_frame = tk.Frame(
            self.window, bg=Settings.getColorPallete()['col1']
        )

        # Load all menu buttons
        self.__load_menu_btn(
            menu_frame, 'Căutare Pacient', self.__search_pacient_btn_action
        )
        self.__load_menu_btn(
            menu_frame, 'Înregistrare Consultație', self.__register_consult_btn_action
        )
        self.__load_menu_btn(
            menu_frame, 'Adăugare Pacient', self.__add_pacient_btn_action
        )

        return menu_frame
    

    # Used in app class to set the menu frame
    def set_menu_scene(self):
        self.__get_menu_frame().grid(
            row=0, column=0, sticky='nsew'
        )
    

    # Sets the content frame for each menu button
    def __set_content_frame(self, frame: tk.Frame):
        if self.current_frame is not None:
            self.current_frame.destroy()
        
        self.current_frame = frame
        self.current_frame.grid(row=1, column=0, sticky='news')

    
    def set_default_content_frame(self, btn_index):
        self.__set_content_frame(self.default_frame)
        self.__highlight_btn(
            btn_index,
            highlight_color=Settings.getColorPallete()['col5'],
            default_color=Settings.getColorPallete()['col1']
        )