import tkinter as tk
from settings import Settings
from database_connector import DatabaseConnector
from tkinter import messagebox   
from gallery_view import GalleryView

class SearchPacientScene:
    def __init__(self, window: tk.Tk) -> None:
        self.window = window
        self.main_frame = tk.Frame(self.window, bg=Settings.getColorPallete()['col5'])

        # Setup main frame grid
        self.main_frame.rowconfigure(0, weight=1)
        self.main_frame.columnconfigure((0, 1), weight=1)

        self.data_frame = tk.Frame(
            self.main_frame, bg='black'
        )

        self.result_frame = tk.Frame(
            self.data_frame,
            bg='black'
        )

        self.scroll_bar = tk.Scrollbar(
            self.result_frame, orient=tk.VERTICAL
        )

        self.pacient_list_box = tk.Listbox(
            self.result_frame, 
            bg=Settings.getColorPallete()['col5'],
            yscrollcommand=self.scroll_bar.set,
            font=Settings.getMainFont(20)
        )
        self.db_connector = DatabaseConnector()
        self.current_pacient_list = []
        self.gallery_index = 0


    def __search_btn_action(self, f_name, l_name, code):
        conditions = []
        if len(f_name):
            conditions.append(f"f_name='{f_name}'")
        if len(l_name):
            conditions.append(f"l_name='{l_name}'")
        if len(code):
            conditions.append(f"cnp='{code}'")

        l = len(conditions)
        # Filter search query based on given conditions
        if l == 0:
            query = 'SELECT * FROM pacients'
        elif l == 1:
            query = f"SELECT * FROM pacients WHERE {conditions[0]}"
        else:
            query = f"SELECT * FROM pacients WHERE {' and '.join(conditions)}"

        # Update pacient list
        self.current_pacient_list = self.db_connector.get_all_data(query)
        
        # Clear list red
        self.pacient_list_box.delete(0, tk.END)
 
        for pacient in self.current_pacient_list:
            pacient_label = (f'Nume: {pacient[1]} | '
                             f'Prenume: {pacient[2]} | '
                             f'CNP: {pacient[3]} | '
                             f'Telefon: {pacient[4]} | '
                             f'Adresa: {pacient[5]} | '
                             f'Email: {pacient[6]} | '
                             )
            
            self.pacient_list_box.insert(tk.END, pacient_label)

        

    def __add_search_frame(self):
        sf = tk.Frame(
            self.main_frame, bg=Settings.getColorPallete()['col5']
        )
        
        # Setup search frame grid
        sf.rowconfigure((0, 1, 2, 3), weight=1)
        sf.rowconfigure(4, weight=3)
        sf.columnconfigure((0, 1), weight=1)
        v_pad = 0
        f_size =20

        # Add labels
        tk.Label(
            sf, text='Filtre(Opțional)',
            bg=Settings.getColorPallete()['col1'], font=Settings.getMainFont(f_size),
            borderwidth=1, relief='solid'
        ).grid(
            row=0, column=0,
            sticky='news',
            pady=(150, v_pad)
        )

        tk.Label(
            sf, text='Nume',
            bg=Settings.getColorPallete()['col5'], font=Settings.getMainFont(f_size),
            borderwidth=1, relief='solid'

        ).grid(
            row=1, column=0,
            sticky='news',
            pady=v_pad
        )

        tk.Label(
            sf, text='Prenume',
            bg=Settings.getColorPallete()['col5'], font=Settings.getMainFont(f_size),
            borderwidth=1, relief='solid'

        ).grid(
            row=2, column=0,
            sticky='news',
            pady=v_pad
        )

        tk.Label(
            sf, text='CNP',
            bg=Settings.getColorPallete()['col5'], font=Settings.getMainFont(f_size),
            borderwidth=1, relief='solid'

        ).grid(
            row=3, column=0,
            sticky='news',
            pady=v_pad
        )

        # Add entries
        f_name_entry = tk.Entry(
            sf, font=Settings.getMainFont(f_size)
        )
        f_name_entry.grid(row=1, column=1, sticky='news', padx=0, pady=v_pad)
        
        l_name_entry = tk.Entry(
            sf, font=Settings.getMainFont(f_size)
        )
        l_name_entry.grid(row=2, column=1, sticky='news', padx=0, pady=v_pad)

        code_entry = tk.Entry(
            sf, font=Settings.getMainFont(f_size)
        )
        code_entry.grid(row=3, column=1, sticky='news', padx=0, pady=v_pad)

        # Add to main frame
        sf.grid(
            row=0, column=0, sticky='news'
        )

        # Add search_btn
        tk.Button(
            sf, text='Căutare', 
            font=f_size + 10, 
            command=lambda: self.__search_btn_action(
                f_name_entry.get(), 
                l_name_entry.get(),
                code_entry.get()
            ), padx=100, pady=20
        ).grid(row=4, column=0, sticky='w')
    

    def __delete_btn_action(self):
        # Skip if no pacient is selected
        if len(self.pacient_list_box.curselection()) == 0:
            return

        if messagebox.askokcancel(
            message="Doresti stergerea acestui pacient"
        ):
            # Delete from database
            list_index = self.pacient_list_box.curselection()[0]
            pacient_id = self.current_pacient_list[list_index][0]
            query = f"DELETE FROM pacients WHERE pacient_id = '{pacient_id}'"
            # Delete selected box entry
            self.pacient_list_box.delete(tk.ANCHOR)
            self.db_connector.execute_query(query)


    def __get_consult_data_list(self, pacient_id):
        query = ("select * "
                "FROM consultations inner join pacients "
                "ON consultations.pacient_id=pacients.pacient_id "
                "where consultations.pacient_id = %s")
        params = (pacient_id,)
        
        return self.db_connector.get_all_data(query, params)


    def __add_history_btn_action(self):
        selection = self.pacient_list_box.curselection()
        if len(selection) == 0:
            return
        pacient_id = self.current_pacient_list[selection[0]][0]
        GalleryView(
            self.main_frame, 
            self.__get_consult_data_list(pacient_id)
        )


    def __add_data_frame(self):
        # Configure data_frame
        self.data_frame.rowconfigure(0, weight=9)
        self.data_frame.rowconfigure(1, weight=1)
        self.data_frame.columnconfigure((0, 1), weight=1)

        # Add data_frame buttons
        tk.Button(
            self.data_frame, text='Ștergere',
            bg='Red', font=Settings.getMainFont(20),
            command=self.__delete_btn_action
        ).grid(
            row=1, column=0, sticky='news',
        )

        tk.Button(
            self.data_frame, text='Istoric',
            bg='skyblue', font=Settings.getMainFont(20),
            command=self.__add_history_btn_action
        ).grid(
            row=1, column=1,sticky='news'
        )

        # Add scrollbar to result frame
        self.scroll_bar.config(
            command=self.pacient_list_box.yview
        )
        self.scroll_bar.pack(side='right', fill='y')

        # Add pacient_list_box to result_frame
        self.pacient_list_box.pack(
            fill='both', expand=True, padx=5
        )

        # Add result_frame to data_frame
        self.result_frame.grid(
            row=0, column=0, 
            columnspan=2, sticky='news'
        )

        # Add to main frame
        self.data_frame.grid(
            row=0, column=1, 
            sticky='news'
        )        
    


    def get_frame(self):
        # Add search frame
        self.__add_search_frame()
        self.__add_data_frame()

        return self.main_frame
    