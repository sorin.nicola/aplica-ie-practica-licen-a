use pacientDB;

create table pacients (
	pacient_id int primary key auto_increment,
    f_name varchar(50) not null,
    l_name varchar(50) not null,
    cnp char(13) not null unique,
    phone_nr char(10) not null,
    adress varchar(50) not null,
    email varchar(50) not null
);

CREATE TABLE consultations (
    consult_id INT PRIMARY KEY AUTO_INCREMENT,
    pacient_id INT,
    registration_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    xray longblob not null,
    diagnostic varchar(50) not null,
    treatment varchar(350) not null,
    observations varchar(350),
    FOREIGN KEY (pacient_id) REFERENCES pacients(pacient_id) ON DELETE CASCADE
);

select * from consultations;
select * from pacients;

